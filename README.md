# 2016 South African municipal elections - City of Tshwane

    start_date: 2016-08-03
    end_date: 2016-08-03
    source: http://www.elections.org.za/content/Elections/Municipal-elections-results/
    wikipedia: https://en.wikipedia.org/wiki/2016_South_African_municipal_elections

**The candidates file might not specify which candidates should be active. If you want any candidates to show up on Open Elections, add a column with the header `active` and add `TRUE` to any candidate you want to include.**